package com.ncrb.samapre.myapplication;

public class PersonMoreDetailsInfo {

	private String ARREST_SURRENDER_DT;
	private String STATE_ENG;
	private String DISTRICT;
	private String PS;
	private String section_code;
	private String occupation;
	private String ARREST_ACTION;
	private String IS_CHRGSHEETED;
	private String CHARGESHEET_NUM;
	private String CHARGESHEET_DT;

	public PersonMoreDetailsInfo(String ARREST_SURRENDER_DT, String STATE_ENG, String DISTRICT, String PS, String section_code, String occupation, String ARREST_ACTION, String IS_CHRGSHEETED, String CHARGESHEET_NUM, String CHARGESHEET_DT) {
		this.ARREST_SURRENDER_DT = ARREST_SURRENDER_DT;
		this.STATE_ENG = STATE_ENG;
		this.DISTRICT = DISTRICT;
		this.PS = PS;
		this.section_code = section_code;
		this.occupation = occupation;
		this.ARREST_ACTION = ARREST_ACTION;
		this.IS_CHRGSHEETED = IS_CHRGSHEETED;
		this.CHARGESHEET_NUM = CHARGESHEET_NUM;
		this.CHARGESHEET_DT = CHARGESHEET_DT;
	}

	public String getARREST_SURRENDER_DT() {
		return ARREST_SURRENDER_DT;
	}

	public void setARREST_SURRENDER_DT(String ARREST_SURRENDER_DT) {
		this.ARREST_SURRENDER_DT = ARREST_SURRENDER_DT;
	}

	public String getSTATE_ENG() {
		return STATE_ENG;
	}

	public void setSTATE_ENG(String STATE_ENG) {
		this.STATE_ENG = STATE_ENG;
	}

	public String getDISTRICT() {
		return DISTRICT;
	}

	public void setDISTRICT(String DISTRICT) {
		this.DISTRICT = DISTRICT;
	}

	public String getPS() {
		return PS;
	}

	public void setPS(String PS) {
		this.PS = PS;
	}

	public String getSection_code() {
		return section_code;
	}

	public void setSection_code(String section_code) {
		this.section_code = section_code;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getARREST_ACTION() {
		return ARREST_ACTION;
	}

	public void setARREST_ACTION(String ARREST_ACTION) {
		this.ARREST_ACTION = ARREST_ACTION;
	}

	public String getIS_CHRGSHEETED() {
		return IS_CHRGSHEETED;
	}

	public void setIS_CHRGSHEETED(String IS_CHRGSHEETED) {
		this.IS_CHRGSHEETED = IS_CHRGSHEETED;
	}

	public String getCHARGESHEET_NUM() {
		return CHARGESHEET_NUM;
	}

	public void setCHARGESHEET_NUM(String CHARGESHEET_NUM) {
		this.CHARGESHEET_NUM = CHARGESHEET_NUM;
	}

	public String getCHARGESHEET_DT() {
		return CHARGESHEET_DT;
	}

	public void setCHARGESHEET_DT(String CHARGESHEET_DT) {
		this.CHARGESHEET_DT = CHARGESHEET_DT;
	}
}