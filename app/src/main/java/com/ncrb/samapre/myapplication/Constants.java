package com.ncrb.samapre.myapplication;

/**
 * Created by Lenovo on 16-02-2018.
 */

public class Constants  {

    // list of services
    public static String mReqSedition = "mReqSedition";

    public static String API_BASE_URL = "http://10.23.72.59:8080/CCTNSSearch2";
    //public static String API_BASE_URL = "http://10.23.74.68/ppSearch/WebService.asmx";

    public static String SearchTypePerson = "Person";
    // for JAVA OR MS STACK ASP.NET
    public static String STACK_JAVA = "STACK_JAVA";
    public static String STACK_MS = "STACK_MS";

    // for JAVA OR MS STACK ASP.NET
    public static String API_PROGRAMMING_STACK = STACK_JAVA;

    public static String SearchTypeProperty = "Property";

    public static String STATE_CODE = "8";//8=delhi, 13=har=yes fir, 27=raj=correct,no fir

}// end
