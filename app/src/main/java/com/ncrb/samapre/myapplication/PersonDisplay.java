package com.ncrb.samapre.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedInput;
import android.app.ProgressDialog;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;


public class PersonDisplay extends AppCompatActivity {

    String P_FName,P_MName,P_LName,P_Alias,P_RelativeName;

    Integer P_GCODE,P_RELGNCODE,P_RELTYPECODE,P_AgeFrom,P_AgeTo,DistCode,PSCode;

    Double P_HEIGHTFROM,P_HEIGHTTO;
    String ChkArr,ChkCon,ChkPO,ChkCS,ChkHO,ChkNA;
    StringBuilder out;
    Integer STATE_CD= Integer.parseInt(Constants.STATE_CODE);
    static Integer CURRENT_PAGE_INDEX=1;
    MCoCoRy mCoCoRy = new  MCoCoRy();

    String Count;
    private ListView listview_person_search;
    TextView title;
    ImageButton btn_Back,btn_Next;
    //,btn_previous;

    public int TOTAL_LIST_ITEMS;
    public int NUM_ITEMS_PAGE   = 15;
    private int noOfBtns;
    private Button[] btns;

    private ArrayList<PersonSearchInfo> arrayList_person_search=new ArrayList<PersonSearchInfo>();
    private ArrayList<PersonSearchInfo> sort=new ArrayList<PersonSearchInfo>();
    private PersonSearchAdapter   personSearchAdapter;
    public ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_display);
        listview_person_search=(ListView)findViewById(R.id.list_person_search_result) ;
        btn_Back = (ImageButton) findViewById(R.id.btn_Back);
        //btn_previous = (ImageButton) findViewById(R.id.btn_Previous);
        btn_Next = (ImageButton) findViewById(R.id.btn_Next);
        title = (TextView)findViewById(R.id.title);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        Bundle bundle = getIntent().getExtras();

        P_FName=bundle.getString("P_FName");
        if (P_FName.isEmpty())
        {
            P_FName="0";
        }
        P_MName=bundle.getString("P_MName");
        if (P_MName.isEmpty())
        {
            P_MName="0";
        }
        P_LName=bundle.getString("P_LName");
        if (P_LName.isEmpty())
        {
            P_LName="0";
        }
        P_Alias=bundle.getString("P_Alias");
        if (P_Alias.isEmpty())
        {
            P_Alias="0";
        }
        P_GCODE=bundle.getInt("P_GCODE");
        P_RELGNCODE=bundle.getInt("P_RELGNCODE");
        P_RELTYPECODE=bundle.getInt("P_RELTYPECODE");
        P_RelativeName=bundle.getString("P_RelativeName");
        if (P_RelativeName.isEmpty())
        {
            P_RelativeName="0";
        }
        P_AgeFrom=bundle.getInt("P_AgeFrom");
        P_AgeTo=bundle.getInt("P_AgeTo");
        P_HEIGHTFROM=bundle.getDouble("P_HEIGHTFROM");
        P_HEIGHTTO=bundle.getDouble("P_HEIGHTTO");
        DistCode=bundle.getInt("Dist_Code");
        PSCode=bundle.getInt("PS_Code");

        Boolean Arr=bundle.getBoolean("Arr");
        if (Arr) {
            ChkArr="true";
        }
        else
        {
            ChkArr="false";
        }

        Boolean Con=bundle.getBoolean("Con");
        if (Con) {
            ChkCon="true";
        }
        else
        {
            ChkCon="false";
        }

        Boolean PO=bundle.getBoolean("PO");
        if (PO) {
            ChkPO="true";
        }
        else
        {
            ChkPO="false";
        }

        Boolean CS=bundle.getBoolean("CS");
        if (CS) {
            ChkCS="true";
        }
        else
        {
            ChkCS="false";
        }
        Boolean HO=bundle.getBoolean("HO");
        if (HO) {
            ChkHO="true";
        }
        else
        {
            ChkHO="false";
        }
        Boolean NA=bundle.getBoolean("NA");
        if (NA) {
            ChkNA="true";
        }
        else
        {
            ChkNA="false";
        }

        try {
            GetAuthDetailWebService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                CURRENT_PAGE_INDEX=CURRENT_PAGE_INDEX+1;
                try {
                    GetAuthDetailWebService();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                View b=findViewById(R.id.btn_Next);
                b.setVisibility(View.INVISIBLE);
            }
        });


        listview_person_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                Intent intent=new Intent(PersonDisplay.this,PersonDetails.class);


                intent.putExtra("FULL_NAME",personSearchAdapter.getItem(position).getFULL_NAME());

                intent.putExtra("ALIAS",personSearchAdapter.getItem(position).getALIAS1());

                intent.putExtra("RELATION_TYPE",personSearchAdapter.getItem(position).getRELATION_TYPE());
                intent.putExtra("RELATIVE_NAME",personSearchAdapter.getItem(position).getRELATIVE_NAME());

                intent.putExtra("REGISTRATION_NUM",personSearchAdapter.getItem(position).getREGISTRATION_NUM());

                intent.putExtra("AccusedSrno",personSearchAdapter.getItem(position).getAccused_srno());



                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }

    public RestAdapter providesRestAdapter() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(480, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(480, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }}).setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }
    public void GetAuthDetailWebService() throws Exception {

        this.mProgressDialog.show();
        String coco_seed = ""; String coco_seed_encd = "";


        try {

            Map postParams = new HashMap();

            if(P_FName.equals(null) || P_FName.equals("0")) P_FName = "0";
            if(P_MName.equals(null) || P_MName.equals("0")) P_MName = "0";
            if(P_LName.equals(null) || P_LName.equals("0")) P_LName = "0";
            if(P_Alias.equals(null) || P_Alias.equals("0")) P_Alias = "0";
            if(P_RelativeName.equals(null) || P_RelativeName.equals("0")) P_RelativeName = "0";



            if(P_GCODE.equals(null)) P_GCODE = 0;
            if(P_RELGNCODE.equals(null)) P_RELGNCODE = 0;
            if(P_RELTYPECODE.equals(null)) P_RELTYPECODE = 0;
            if(P_AgeFrom.equals(null)) P_AgeFrom = 0;
            if(P_AgeTo.equals(null)) P_AgeTo = 0;
            if(P_HEIGHTFROM.equals(null)) P_HEIGHTFROM = 0.0;
            if(P_HEIGHTTO.equals(null)) P_HEIGHTTO = 0.0;
            if(DistCode.equals(null)) DistCode = 0;
            if(PSCode.equals(null)) PSCode = 0;


            postParams.put("MATCH_CRITERIA_ARR",ChkArr.toString());
            postParams.put("MATCH_CRITERIA_CON",ChkCon.toString());
            postParams.put("MATCH_CRITERIA_PO",ChkPO.toString());
            postParams.put("MATCH_CRITERIA_CS",ChkCS.toString());
            postParams.put("MATCH_CRITERIA_HO",ChkHO.toString());
            postParams.put("MATCH_CRITERIA_NOTARR",ChkNA.toString());

            postParams.put("P_FName",P_FName.toString());
            postParams.put("P_MName",P_MName.toString());
            postParams.put("P_LName",P_LName.toString());
            postParams.put("P_Alias",P_Alias.toString());

            postParams.put("P_GCODE",P_GCODE);
            postParams.put("P_RELGNCODE",P_RELGNCODE);
            postParams.put("P_RELTYPECODE",P_RELTYPECODE);

            postParams.put("P_RelativeName",P_RelativeName.toString());

            postParams.put("P_AgeFrom",P_AgeFrom);
            postParams.put("P_AgeTo",P_AgeTo);
            postParams.put("P_HEIGHTFROM",P_HEIGHTFROM);
            postParams.put("P_HEIGHTTO",P_HEIGHTTO);
            postParams.put("STATE_CD",STATE_CD);
            postParams.put("DISTRICT_CD",DistCode);
            postParams.put("PS_CD",PSCode);


            postParams.put("CURRENT_PAGE_INDEX",CURRENT_PAGE_INDEX);
            postParams.put("m_service","mPersonSearch");


            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);


            Utils.printv("post params without encode "+coco_seed);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);
        Utils.printv("post params "+postParams);
        JSONPostParams jsonPostParams = new JSONPostParams("mPersonSearch", postParams);


        // -----------------------------------------------------------------

        RestAdapter restAdapter =providesRestAdapter();


        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        int cnt;
        apiCaller.mPersonSearch(jsonPostParams, new Callback<WSPLoginConnect>() {

                    @Override
                    public void failure(RetrofitError arg0) {
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


                    }// end failure

                    @Override
                    public void success(WSPLoginConnect result2, Response response) {
                        // 1. convert seed into string
                        // 2 .convert string into json

                        String jsonString = "";

                        if(!result2.seed.equals("")) {

                            jsonString = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), result2.seed, "DECODE");

                            if(jsonString.equals("")) {
                                Toast.makeText(getApplicationContext(), "System error, please contact administrator.", Toast.LENGTH_LONG).show();
                                return;
                            }

                        }

                        Gson gson = new Gson();


                        WSPLoginConnect result = gson.fromJson(jsonString, WSPLoginConnect.class);
                        if (result.STATUS_CODE.toString().equals("200")) {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();


                            try {

                                JSONObject reader = new JSONObject(jsonString);
                                JSONArray states = reader.getJSONArray("personSearchCheck");

                                arrayList_person_search.clear();
                                for (int i = 0; i < states.length(); i++) {

                                    JSONObject jsonObj2 = states.getJSONObject(i);

                                    Gson gson1 = new Gson();
                                    PersonSearchInfo objPersonSearchInfo = gson1.fromJson(jsonObj2.toString(), PersonSearchInfo.class);

                                    arrayList_person_search.add(objPersonSearchInfo);







                                } //end forloop

                                Btnfooter((int)states.length(),CURRENT_PAGE_INDEX);
                                loadList(CURRENT_PAGE_INDEX-1);
                                CheckBtnBackGroud(0,CURRENT_PAGE_INDEX);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//new String(jsonObj2.getString("NATIONALITY")),


                            System.out.println("RESULT status " + result.STATUS_CODE);







                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                        }
                    }// end success

                }

        );

    }

    private void Btnfooter(int count,int CURRENT_PAGE_INDEX)
    {

        TOTAL_LIST_ITEMS=count;
        int val = TOTAL_LIST_ITEMS%NUM_ITEMS_PAGE;
        val = val==0?0:1;
        noOfBtns=TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE+val;

        LinearLayout ll = (LinearLayout)findViewById(R.id.btnLay);


        int k=(CURRENT_PAGE_INDEX*10)-9;
        if(CURRENT_PAGE_INDEX==1)
            btns = new Button[noOfBtns];
        for(int i=0;i<noOfBtns;i++)
        {
            if(CURRENT_PAGE_INDEX==1)
                btns[i] =   new Button(this);
            btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            // btns[i].setText(""+(i+1));
            btns[i].setText(""+k);
            k=k+1;


            LinearLayout.LayoutParams lp = new     LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            if (CURRENT_PAGE_INDEX==1)
                ll.addView(btns[i], lp);

            final int j = i;
            final int cpi=CURRENT_PAGE_INDEX;

            btns[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadList(j);
                    CheckBtnBackGroud(j,cpi);

                    check(j);
                }

            });
        }

    }

    private void check(int j) {
        if ((j != 0) && ((j+1) % 10 == 0)) {

            View b=findViewById(R.id.btn_Next);
            b.setVisibility(View.VISIBLE);
        }
    }

    private void loadList(int number)
    {
        sort.clear();
        //ArrayList<String> sort = new PropertySearchInfo(String, String, String reg_Owner, String MV_Type, String REGISTRATION_NO, String CHASSIS_NO, String ENGINE_NO, String full_FIR_NUMBER, String state, String district, String ps, String propertyNature, String mvModel, String mvMake, String mvColor);
        int start = number * NUM_ITEMS_PAGE;
        for(int i=start;i<(start)+NUM_ITEMS_PAGE;i++)
        {
            if(i<arrayList_person_search.size())
            {
                sort.add(arrayList_person_search.get(i));
            }
            else
            {
                break;
            }
        }
        personSearchAdapter= new PersonSearchAdapter(getApplicationContext(),sort);
        listview_person_search.setAdapter(personSearchAdapter);
    }


    private void CheckBtnBackGroud(int index,int cpi)
    {
        int temp=(cpi*10)-10;
        temp=temp+index+1;
        title.setText("Page "+ temp );
        for(int i=0;i<noOfBtns;i++)
        {
            if(i==index)
            {

                btns[i].setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            else
            {
                btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns[i].setTextColor(getResources().getColor(android.R.color.black));
            }
        }

    }
}
