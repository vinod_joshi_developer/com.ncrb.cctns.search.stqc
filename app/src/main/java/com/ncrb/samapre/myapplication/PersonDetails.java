package com.ncrb.samapre.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.util.Base64;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedInput;


public class PersonDetails extends AppCompatActivity {
    ImageButton btn_back;

    TextView popuptext;
    Button btn_popCancel;
    PopupWindow pw;
    MCoCoRy mCoCoRy = new  MCoCoRy();
    AppPreferences objAppPreferences;
    Singleton singleton;
    public ProgressDialog mProgressDialog;
    StringBuilder out;

    private ArrayList<PersonMoreDetailsInfo> arrayList_person_more_details=new ArrayList<PersonMoreDetailsInfo>();

    String FullRegnum,AccusedSrno;
    String FIR_REG_NUM_detail, ACCUSED_SRNO_detail;

    String GENDER="", AGE="", RELIGION="", HEIGHT_FROM_CM="", HEIGHT_TO_CM="", UID_NUM="";
    String STATE_ENG="", DISTRICT="", PS="", REG_NUM="", REG_DT="", PERMANENT="";
    String PRESENT="", UploadedFile="",section="",  AllAccusedNames="",DISTRICT_CD="",PS_CD="",FIR_NUMBER_ONLY="",FIR_YEAR_ONLY="";

    ImageView accusedimage;
    Bundle data;

    TextView textView_person_full_name, textView_person_alias, textView_person_relation_type;
    TextView textView_person_gender, textView_person_age, textView_person_religion, txt_person_Act_Section, txt_person_All_Accused;
    //textView_person_nationality;
    TextView textView_person_height_from, textView_person_height_to, textView_person_uid;
    TextView textView_person_State, textView_person_district, textView_person_ps;
    TextView textView_person_FIR_reg_num, textView_person_full_FIR_reg_num, textView_person_FIR_date;
    TextView textView_person_relative_name, textView_person_permanent, textView_person_present;
    // textView_person_type

    public String BASE64IMAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);

        singleton = Singleton.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");
        // core objects
        objAppPreferences = new AppPreferences(PersonDetails.this);

        data = getIntent().getExtras();

        // update count value
        updateUserCount(PersonDetails.this, Constants.SearchTypePerson);

        Button btn_pdf, btn_more_details, btn_view_fir,btn_popup;

        btn_back = (ImageButton) findViewById(R.id.btn_Back);
        btn_pdf = (Button) findViewById(R.id.btn_pdf);
        btn_more_details = (Button) findViewById(R.id.btn_more_details);
        btn_view_fir = (Button) findViewById(R.id.btn_view_fir);
        //btn_popup=(Button)findViewById(R.id.btn_popup);

        textView_person_full_name = (TextView) findViewById(R.id.txt_person_detail_Full_Name);
        textView_person_alias = (TextView) findViewById(R.id.txt_person_detail_alias);
        textView_person_relation_type = (TextView) findViewById(R.id.txt_person_detail_Relation_Type);
        textView_person_relative_name = (TextView) findViewById(R.id.txt_person_detail_Relative_Name);
        textView_person_gender = (TextView) findViewById(R.id.txt_person_detail_Gender);
        textView_person_age = (TextView) findViewById(R.id.txt_person_detail_age);
        textView_person_religion = (TextView) findViewById(R.id.txt_person_detail_religion);
        // textView_person_nationality=(TextView)findViewById(R.id.txt_person_detail_nationality);
        textView_person_height_from = (TextView) findViewById(R.id.txt_person_detail_Height_From);
        textView_person_height_to = (TextView) findViewById(R.id.txt_person_detail_Height_To);
        textView_person_uid = (TextView) findViewById(R.id.txt_person_detail_UID);
        textView_person_State = (TextView) findViewById(R.id.txt_person_detail_State);
        textView_person_district = (TextView) findViewById(R.id.txt_person_detail_district);
        textView_person_ps = (TextView) findViewById(R.id.txt_person_detail_ps);
        textView_person_FIR_reg_num = (TextView) findViewById(R.id.txt_person_detail_fir_no);
        //textView_person_full_FIR_reg_num=(TextView)findViewById(R.id.txt_person_detail_Full_FIR_NUMBER);
        textView_person_FIR_date = (TextView) findViewById(R.id.txt_person_detail_FIR_Date);
        //textView_person_type = (TextView) findViewById(R.id.txt_person_detail_type);
        textView_person_permanent = (TextView) findViewById(R.id.txt_person_Permanent_Address);
        textView_person_present = (TextView) findViewById(R.id.txt_person_Present_Address);
        txt_person_Act_Section = (TextView) findViewById(R.id.txt_person_Act_Section);
        txt_person_All_Accused = (TextView) findViewById(R.id.txt_person_All_Accused);
        accusedimage = (ImageView) findViewById(R.id.accusedimage);


//        // todo remove static
//        FullRegnum = FIR_REG_NUM_detail="27541007172013";
//        AccusedSrno = ACCUSED_SRNO_detail="27541007170000808";


        FullRegnum = FIR_REG_NUM_detail=data.getString("REGISTRATION_NUM");
        AccusedSrno = ACCUSED_SRNO_detail=data.getString("AccusedSrno");

        System.out.println("REG NUM & Accused: "+data.getString("REGISTRATION_NUM")+data.getString("AccusedSrno"));

        try {
            GetPersonDetailDisplayWebService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        textView_person_full_name.setText(data.getString("FULL_NAME"));
        textView_person_alias.setText(data.getString("ALIAS"));
        textView_person_relation_type.setText(data.getString("RELATION_TYPE"));
        textView_person_relative_name.setText(data.getString("RELATIVE_NAME"));

        // accusedimage.setImageBitmap(decodedByte);

// find image of person


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPDF(view);
            }
        });

        btn_more_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(FullRegnum==null || TextUtils.isEmpty(FullRegnum) ||AccusedSrno==null||TextUtils.isEmpty(AccusedSrno))
                {
                    Toast.makeText(getApplicationContext(), "No more Data Available...", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intentmore = new Intent(getApplicationContext(), PersonMoreDetails.class);
                    Bundle bundle = new Bundle();

                    //bundle.putString("ARREST_SURRENDER_DT", ARREST_SURRENDER_DT);
                    bundle.putString("REGISTRATION_NUM", FullRegnum);
                    bundle.putString("AccusedSrno", AccusedSrno);

                    // bundle.putString("STT", arrayList_person_more_details.get(1).getSTATE_ENG().toString());

                    intentmore.putExtras(bundle);

                    startActivity(intentmore);
                }
            }

        });


        btn_view_fir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check();

            }

        });

    }// end oncreate

    private void check() {

        //todo static remove
        //FullRegnum = "0001";

        if(FullRegnum==null ||FullRegnum==""|| AccusedSrno==null||AccusedSrno=="")
        {
            Toast.makeText(getApplicationContext(), "No Data Available...", Toast.LENGTH_LONG).show();
        }

        else {




            //todo static remove
//            singleton.firregnum = "0001";// fir number 4 digit only
//            singleton.district_cd = "13240";
//            singleton.ps_cd = "13240012";
//            singleton.year = "2016";// year of fir


            singleton.firregnum = FIR_NUMBER_ONLY;// fir number 4 digit only
            singleton.district_cd = DISTRICT_CD;
            singleton.ps_cd = PS_CD;
            singleton.year = FIR_YEAR_ONLY;// year of fir

            try {

                Intent intentmore = new Intent(getApplicationContext(), FirInputs.class);
                startActivity(intentmore);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }



    public void printPDF(View view) {
        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);
        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this,findViewById(R.id.pdfLayout)), null);
    }

    // create and update count dynamically
    public void updateUserCount(Context context, String searchType) {


        String date = Utils.getDateTime("dd-MM-yyyy");

        String uniqueKey = searchType+" "+date+" "+this.singleton.username;

        try {

            // core objects
            objAppPreferences = new AppPreferences(context);

            String checkUser = objAppPreferences.getUserDefaults(uniqueKey);

            if(!checkUser.equals("")) {
                int count = Integer.parseInt(checkUser);
                count++;
                objAppPreferences.setUserDefaults(uniqueKey, ""+count);
                Utils.printv("Set Preference "+uniqueKey+"count: "+count);
            } else {
                objAppPreferences.setUserDefaults(uniqueKey, "1");
                Utils.printv("Set Preference "+uniqueKey+"count: 1");
            }

        }catch (Exception e){
            e.printStackTrace();
            objAppPreferences.setUserDefaults(uniqueKey, "1");
            Utils.printv("Set Preference "+uniqueKey+"count: 1");
        }

    }// end update count


    public RestAdapter providesRestAdapter() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(480, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(480, TimeUnit.SECONDS);

        return new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }}).setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    public void GetPersonDetailDisplayWebService() throws Exception {
        this.mProgressDialog.show();
        String coco_seed = ""; String coco_seed_encd = "";


        try {

            Map postParams = new HashMap();

            postParams.put("FIR_REG_NUM_detail",FIR_REG_NUM_detail.toString());
            postParams.put("ACCUSED_SRNO_detail",ACCUSED_SRNO_detail.toString());
            postParams.put("m_service","mPersonDetailDisplay");


            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);


            Utils.printv("post params without encode "+coco_seed);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);
        Utils.printv("post params "+postParams);
        JSONPostParams jsonPostParams = new JSONPostParams("mPersonDetailDisplay", postParams);


        // -----------------------------------------------------------------

        RestAdapter restAdapter =providesRestAdapter();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        int cnt;
        apiCaller.mPersonDetailDisplay(jsonPostParams, new Callback<WSPLoginConnect>() {

                    @Override
                    public void failure(RetrofitError arg0) {
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();

                    }// end failure

                    @Override
                    public void success(WSPLoginConnect result2, Response response) {
                        // 1. convert seed into string
                        // 2 .convert string into json

                        String jsonString = "";

                        if(!result2.seed.equals("")) {

                            jsonString = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), result2.seed, "DECODE");

                            if(jsonString.equals("")) {
                                Toast.makeText(getApplicationContext(), "System error, please contact administrator.", Toast.LENGTH_LONG).show();
                                return;
                            }

                        }

                        Gson gson = new Gson();


                        WSPLoginConnect result = gson.fromJson(jsonString, WSPLoginConnect.class);

                        if (result.STATUS_CODE.toString().equals("200")) {


                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();

                            try {
                                JSONObject reader = new JSONObject(jsonString);
                                JSONArray states = reader.getJSONArray("PersonDetailDisplayList");

                                for (int i = 0; i < states.length(); i++) {
                                    JSONObject jsonObj2 = states.getJSONObject(i);


                                    if (states.getJSONObject(0).has("GENDER"))
                                        GENDER=states.getJSONObject(0).getString("GENDER");
                                    if (states.getJSONObject(0).has("AGE"))
                                        AGE=states.getJSONObject(0).getString("AGE");
                                    if (states.getJSONObject(0).has("RELIGION"))
                                        RELIGION=states.getJSONObject(0).getString("RELIGION");
                                    if (states.getJSONObject(0).has("HEIGHT_FROM_CM"))
                                        HEIGHT_FROM_CM=states.getJSONObject(0).getString("HEIGHT_FROM_CM");
                                    if (states.getJSONObject(0).has("HEIGHT_TO_CM"))
                                        HEIGHT_TO_CM=states.getJSONObject(0).getString("HEIGHT_TO_CM");
                                    if (states.getJSONObject(0).has("UID_NUM"))
                                        UID_NUM=states.getJSONObject(0).getString("UID_NUM");
                                    if (states.getJSONObject(0).has("STATE_ENG"))
                                        STATE_ENG=states.getJSONObject(0).getString("STATE_ENG");
                                    if (states.getJSONObject(0).has("DISTRICT"))
                                        DISTRICT=states.getJSONObject(0).getString("DISTRICT");
                                    if (states.getJSONObject(0).has("PS"))
                                        PS=states.getJSONObject(0).getString("PS");
                                    if (states.getJSONObject(0).has("REG_NUM"))
                                        REG_NUM=states.getJSONObject(0).getString("REG_NUM");
                                    if (states.getJSONObject(0).has("REG_DT"))
                                        REG_DT=states.getJSONObject(0).getString("REG_DT");
                                    if (states.getJSONObject(0).has("PERMANENT"))
                                        PERMANENT=states.getJSONObject(0).getString("PERMANENT");
                                    if (states.getJSONObject(0).has("PRESENT"))
                                        PRESENT=states.getJSONObject(0).getString("PRESENT");
                                    if (states.getJSONObject(0).has("UploadedFile"))
                                        UploadedFile=states.getJSONObject(0).getString("UploadedFile");
                                    if (states.getJSONObject(0).has("Section"))
                                        section=states.getJSONObject(0).getString("Section");
                                    if (states.getJSONObject(0).has("all_accused"))
                                        AllAccusedNames=states.getJSONObject(0).getString("all_accused");
                                    if (states.getJSONObject(0).has("DISTRICT_CD"))
                                        DISTRICT_CD=states.getJSONObject(0).getString("DISTRICT_CD");
                                    if (states.getJSONObject(0).has("PS_CD"))
                                        PS_CD=states.getJSONObject(0).getString("PS_CD");
                                    if (states.getJSONObject(0).has("FIR_NUMBER_ONLY"))
                                        FIR_NUMBER_ONLY=states.getJSONObject(0).getString("FIR_NUMBER_ONLY");
                                    if (states.getJSONObject(0).has("FIR_YEAR_ONLY"))
                                        FIR_YEAR_ONLY=states.getJSONObject(0).getString("FIR_YEAR_ONLY");


                                } //end forloop

                                System.out.println("AllAccusedNames: "+AllAccusedNames);

                                textView_person_gender.setText(GENDER.equals(null) ? "" : GENDER);
                                textView_person_age.setText(AGE.equals(null) ? "" : AGE);
                                textView_person_religion.setText(RELIGION.equals(null) ? "" : RELIGION);

                                textView_person_height_from.setText(HEIGHT_FROM_CM.equals(null) ? "" : HEIGHT_FROM_CM);
                                textView_person_height_to.setText(HEIGHT_TO_CM.equals(null) ? "" : HEIGHT_TO_CM);
                                textView_person_uid.setText(UID_NUM.equals(null) ? "" : UID_NUM);
                                textView_person_State.setText(STATE_ENG.equals(null) ? "" : STATE_ENG);
                                textView_person_district.setText(DISTRICT.equals(null) ? "" : DISTRICT);
                                textView_person_ps.setText(PS.equals(null) ? "" : PS);
                                textView_person_FIR_reg_num.setText(REG_NUM.equals(null) ? "" : REG_NUM);

                                textView_person_FIR_date.setText(REG_DT.equals(null) ? "" : REG_DT);

                                textView_person_permanent.setText(PERMANENT.equals(null) ? "" : PERMANENT);
                                textView_person_present.setText(PRESENT.equals(null) ? "" : PRESENT);

                                // UploadedFile = UploadedFile;

                                txt_person_Act_Section.setText(section.equals(null) ? "" : section);
                                txt_person_All_Accused.setText(AllAccusedNames.equals(null) ? "" : AllAccusedNames);



                                if (!UploadedFile.equals("")) {

//                                    // added for java
//                                    if (!BASE64IMAGE.equals("")) {
//
//                                        byte[] decodedString = Base64.decode(BASE64IMAGE, Base64.DEFAULT);
//                                        // System.out.println("getimage="+decodedString);
//                                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//                                        System.out.println("decoded byte=" + decodedByte);
//                                        accusedimage.setImageBitmap(decodedByte);
//                                    }

                                    // for .NET VERSION

                                    byte[] decodedString = Base64.decode(UploadedFile, Base64.DEFAULT);
                                    // System.out.println("getimage="+decodedString);
                                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                    System.out.println("decoded byte=" + decodedByte);
                                    accusedimage.setImageBitmap(decodedByte);
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//new String(jsonObj2.getString("NATIONALITY")),

                            System.out.println("RESULT status " + result.STATUS_CODE);


                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();


                        }
                    }// end success

                }

        );

    }
}
